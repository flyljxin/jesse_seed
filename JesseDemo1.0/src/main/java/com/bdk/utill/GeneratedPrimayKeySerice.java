package com.bdk.utill;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import org.joda.time.DateTime;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneratedPrimayKeySerice {

	final static Logger logger = LoggerFactory.getLogger(GeneratedPrimayKeySerice.class);
	
	public static String serviceNodeId;
	private final static String DATE_FORMAT = "yyyyMMddHHmmssSSS";
	private final static String SIMPLE_DATE_FORMAT = "yyyyMMddHHmmss";
	private final static AtomicLong idIncreaseCounter = new AtomicLong();

	private final static String COMMISSION_CHANGE_CODE_PREFIX = "YJBG";
	private final static String BALANCE_CHANGE_CODE_PREFIX = "CYTZ";
	private final static String BILL_SYS_CODE_PREFIX = "FP";

	/**
	 * 生成主键ID
	 * 
	 * @return
	 */
	public static String getGeneratedPk() {
		logger.info("生成主键id");
		String dataStr = new DateTime(new Date()).toString(DATE_FORMAT);
		long num = idIncreaseCounter.getAndIncrement();
		String rondomNum = String.format("%4d", num).replace(" ", "0");
		if (num >= 9999) {
			idIncreaseCounter.set(0);
		}
		String primaryKey = serviceNodeId + dataStr + rondomNum + "000000";
		return primaryKey;
	}

	/**
	 * 生成合同交易编码
	 * 
	 * @return
	 */
//	public static String getGenerateTradeCode() {
//		logger.info("生成合同交易编码");
//		String rondomNum = String.format("%4d", ((int) (Math.random() * 100000) / 10)).replace(" ", "0");
//		String dataStr = new DateTime(new Date()).toString(SIMPLE_DATE_FORMAT);
//		UserContextVO userVO = FacadeContext.getCurrentUser();
//		String citysimple = "";
//		if (userVO != null) {
//			// 获取城市简写
//			citysimple = CacheAccessingUtil.getCacheValueByKey(userVO.getCityId(), CityVO.class, "citysimplespell");
//		}
//		return citysimple + serviceNodeId + dataStr + rondomNum;
//	}

	

	public static void setServiceNodeId(String serviceNodeId) {
		GeneratedPrimayKeySerice.serviceNodeId = serviceNodeId;
	}

	/**
	 * 获取佣金变更的编码
	 * 
	 * @return
	 */
//	public static String getCommissionChangeCode() {
//		logger.info("生成佣金变更编码");
//		String rondomNum = String.format("%4d", ((int) (Math.random() * 100000) / 10)).replace(" ", "0");
//		String dataStr = DateUtil.convertDateToString(new Date(), SIMPLE_DATE_FORMAT);
//		UserContextVO userVO = FacadeContext.getCurrentUser();
//		StringBuilder sb = new StringBuilder(COMMISSION_CHANGE_CODE_PREFIX);
//		sb.append("-");
//		if (userVO != null) {
//			// 获取城市简写
//			sb.append(CacheAccessingUtil.getCacheValueByKey(userVO.getCityId(), CityVO.class, "citysimplespell"));
//		}
//		sb.append(serviceNodeId).append(dataStr).append(rondomNum);
//		return sb.toString();
//	}

}
