package com.bdk.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.bdk.conmon.ResultCode;
import com.bdk.conmon.ResultEntity;
import com.bdk.conmon.SystemCons;
import com.bdk.mapper.Role;
import com.bdk.mapper.User;
import com.bdk.service.IRoleService;
import com.bdk.service.IUserService;
import com.bdk.utill.JSONUtils;
import com.bdk.utill.StringHelper;

/**
 * 用户相关操作
 * ClassName: UserController 
 * @Description: 
 * @author ChenQuan
 * @date 2017年5月4日下午1:55:49
 */
@Controller
@RequestMapping("/role")
public class RoleController {
	
	final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private IRoleService roleService;
	
	//角色主页
	@RequestMapping("/roleIndex.do")
	public String roleIndex(HttpServletRequest request, Role role, Model model) {
		String queryString = request.getParameter("queryString");
	    
		if(SystemCons.checkLoging(request))
		{
			List<Role> periodList = roleService.queryUserList(queryString,role.getCurrPage(),role.getPageNums());
			
			model.addAttribute("userList", periodList);
			model.addAttribute("total", roleService.queryTotal(queryString));
			model.addAttribute("currPage", role.getCurrPage());
			model.addAttribute("pageNums", role.getPageNums());
			model.addAttribute("queryString", queryString);
			
			logger.info("执行查询用户列表操作");
			return "/admin/role/role";
		}
		return "redirect:/user/loginParent.do";
	}
}
