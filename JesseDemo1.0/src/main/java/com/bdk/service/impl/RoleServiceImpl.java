package com.bdk.service.impl;

import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bdk.dao.RoleMapper;
import com.bdk.mapper.Role;
import com.bdk.service.IRoleService;

/**
 * 用户相关实现 ClassName: UserServiceImpl
 * 
 * @Description:
 * @author ChenQuan
 * @date 2017年5月4日下午1:57:23
 */
@Service("roleService")
public class RoleServiceImpl implements IRoleService {
	@Resource
	private RoleMapper roleDao;

	/**
	 * 根据 id 删除 数据
	 */
	public int delete(int id) {
		return roleDao.delete(id);
	}

	/**
	 * 查询User的全部数据
	 */
	public List<Role> findAll() {
		List<Role> findAllList = roleDao.findAll();
		return findAllList;
	}

	/**
	 * 根据 id 查询 对应数据
	 */
	public Role findById(int id) {
		Role user = roleDao.findById(id);
		return user;
	}

	/**
	 * 新增数据
	 */
	public void save(Role user) {
		user.setRoleId(UUID.randomUUID().toString());
		roleDao.insertSelective(user);
	}

	/**
	 * 根据 id 修改对应数据
	 */
	public int update(Role user) {
		return roleDao.update(user);
	}

	@Override
	public Role login(String name, String pwd) {
		return roleDao.login(name, pwd);
	}

	/*@Override
	public void updatePassword(Role user) {
		if (user == null || StringUtils.isBlank(user.getId())) {
			return;
		}

		roleDao.update(user);
	}*/

	@Override
	public List<Role> queryUserList(String userName, int pageNo, int pageSize) {
		return roleDao.queryUserList(userName, (pageNo - 1) * pageSize, pageSize);
	}

	@Override
	public int queryTotal(String userName) {
		return roleDao.queryTotal(userName);
	}

	@Override
	public int queryUserName(String userName) {
		return roleDao.queryUserName(userName);
	}

	@Override
	public void updatePassword(Role role) {
		// TODO Auto-generated method stub
		
	}
}
