/*     */ package com.wu.common.threadpool.cached;
/*     */ 
/*     */ import com.wu.common.threadpool.AbortPolicyWithReport;
/*     */ import com.wu.common.threadpool.NamedThreadFactory;
/*     */ import com.wu.common.threadpool.ThreadPool;

/*     */ import java.util.concurrent.BlockingQueue;
/*     */ import java.util.concurrent.Executor;
/*     */ import java.util.concurrent.LinkedBlockingQueue;
/*     */ import java.util.concurrent.RejectedExecutionHandler;
/*     */ import java.util.concurrent.SynchronousQueue;
/*     */ import java.util.concurrent.ThreadFactory;
/*     */ import java.util.concurrent.ThreadPoolExecutor;
/*     */ import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.InitializingBean;
/*     */ 
/*     */ public class CachedThreadPool
/*     */   implements ThreadPool, InitializingBean
/*     */ {
/*  29 */   private static Logger logger = Logger.getLogger(CachedThreadPool.class);
/*     */ 
/*  31 */   private String name = "WU";
/*     */ 
/*  33 */   private int cores = 0;
/*     */ 
/*  35 */   private int threads = 200;
/*     */ 
/*  37 */   private int queues = 0;
/*     */ 
/*  39 */   private int alive = 60000;
/*     */ 
/*  41 */   private RejectedExecutionHandler rejectedHandler = new AbortPolicyWithReport(this.name);
/*     */ 
/*  43 */   private ThreadFactory threadFactory = new NamedThreadFactory(this.name, true);
/*     */   private Executor executor;
/*     */ 
/*     */   public void initialize()
/*     */   {
/*  53 */     this.executor = new ThreadPoolExecutor(this.cores, this.threads, this.alive, TimeUnit.MILLISECONDS, (BlockingQueue)(this.queues < 0 ? new LinkedBlockingQueue() : this.queues == 0 ? new SynchronousQueue() : new LinkedBlockingQueue(this.queues)), this.threadFactory, this.rejectedHandler);
/*     */   }
/*     */ 
/*     */   public Executor getExecutor()
/*     */   {
/*  62 */     return this.executor;
/*     */   }
/*     */ 
/*     */   public void execute(Runnable command)
/*     */   {
/*  67 */     this.executor.execute(command);
/*     */   }
/*     */ 
/*     */   public void afterPropertiesSet() throws Exception
/*     */   {
/*  72 */     initialize();
/*     */   }
/*     */ 
/*     */   public String getName() {
/*  76 */     return this.name;
/*     */   }
/*     */ 
/*     */   public void setName(String name) {
/*  80 */     this.name = name;
/*     */   }
/*     */ 
/*     */   public int getCores() {
/*  84 */     return this.cores;
/*     */   }
/*     */ 
/*     */   public void setCores(int cores) {
/*  88 */     this.cores = cores;
/*     */   }
/*     */ 
/*     */   public int getThreads() {
/*  92 */     return this.threads;
/*     */   }
/*     */ 
/*     */   public void setThreads(int threads) {
/*  96 */     this.threads = threads;
/*     */   }
/*     */ 
/*     */   public int getQueues() {
/* 100 */     return this.queues;
/*     */   }
/*     */ 
/*     */   public void setQueues(int queues) {
/* 104 */     this.queues = queues;
/*     */   }
/*     */ 
/*     */   public int getAlive() {
/* 108 */     return this.alive;
/*     */   }
/*     */ 
/*     */   public void setAlive(int alive) {
/* 112 */     this.alive = alive;
/*     */   }
/*     */ 
/*     */   public RejectedExecutionHandler getRejectedHandler() {
/* 116 */     return this.rejectedHandler;
/*     */   }
/*     */ 
/*     */   public void setRejectedHandler(RejectedExecutionHandler rejectedHandler) {
/* 120 */     this.rejectedHandler = rejectedHandler;
/*     */   }
/*     */ 
/*     */   public ThreadFactory getThreadFactory() {
/* 124 */     return this.threadFactory;
/*     */   }
/*     */ 
/*     */   public void setThreadFactory(ThreadFactory threadFactory) {
/* 128 */     this.threadFactory = threadFactory;
/*     */   }
/*     */ 
/*     */   public void setExecutor(Executor executor) {
/* 132 */     this.executor = executor;
/*     */   }
/*     */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.cached.CachedThreadPool
 * JD-Core Version:    0.6.0
 */