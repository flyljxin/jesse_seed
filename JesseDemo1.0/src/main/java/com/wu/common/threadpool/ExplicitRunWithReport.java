/*    */ package com.wu.common.threadpool;
/*    */ 
/*    */ import java.util.concurrent.ThreadPoolExecutor;
/*    */ import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;

import org.apache.log4j.Logger;
/*    */ 
/*    */ public class ExplicitRunWithReport extends ThreadPoolExecutor.AbortPolicy
/*    */ {
/* 17 */   protected static final Logger logger = Logger.getLogger(AbortPolicyWithReport.class);
/*    */   private final String threadName;
/*    */ 
/*    */   public ExplicitRunWithReport(String threadName)
/*    */   {
/* 22 */     this.threadName = threadName;
/*    */   }
/*    */ 
/*    */   public void rejectedExecution(Runnable r, ThreadPoolExecutor e)
/*    */   {
/* 27 */     String msg = String.format("Thread pool is EXHAUSTED! Thread Name: %s, Pool Size: %d (active: %d, core: %d, max: %d, largest: %d), Task: %d (completed: %d), Executor status:(isShutdown:%s, isTerminated:%s, isTerminating:%s)", new Object[] { this.threadName, Integer.valueOf(e.getPoolSize()), Integer.valueOf(e.getActiveCount()), Integer.valueOf(e.getCorePoolSize()), Integer.valueOf(e.getMaximumPoolSize()), Integer.valueOf(e.getLargestPoolSize()), Long.valueOf(e.getTaskCount()), Long.valueOf(e.getCompletedTaskCount()), Boolean.valueOf(e.isShutdown()), Boolean.valueOf(e.isTerminated()), Boolean.valueOf(e.isTerminating()) });
/*    */ 
/* 32 */     logger.warn(msg);
/* 33 */     r.run();
/*    */   }
/*    */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.ExplicitRunWithReport
 * JD-Core Version:    0.6.0
 */