/*    */ package com.wu.common.threadpool;
/*    */ 
/*    */ import java.util.concurrent.ThreadFactory;
/*    */ import java.util.concurrent.atomic.AtomicInteger;
/*    */ 
/*    */ public class NamedThreadFactory
/*    */   implements ThreadFactory
/*    */ {
/* 14 */   private static final AtomicInteger POOL_SEQ = new AtomicInteger(1);
/*    */ 
/* 16 */   private final AtomicInteger mThreadNum = new AtomicInteger(1);
/*    */   private final String mPrefix;
/*    */   private final boolean mDaemo;
/*    */   private final ThreadGroup mGroup;
/*    */ 
/*    */   public NamedThreadFactory()
/*    */   {
/* 26 */     this("pool-" + POOL_SEQ.getAndIncrement(), false);
/*    */   }
/*    */ 
/*    */   public NamedThreadFactory(String prefix)
/*    */   {
/* 31 */     this(prefix, false);
/*    */   }
/*    */ 
/*    */   public NamedThreadFactory(String prefix, boolean daemo)
/*    */   {
/* 36 */     this.mPrefix = (prefix + "-thread-");
/* 37 */     this.mDaemo = daemo;
/* 38 */     SecurityManager s = System.getSecurityManager();
/* 39 */     this.mGroup = (s == null ? Thread.currentThread().getThreadGroup() : s.getThreadGroup());
/*    */   }
/*    */ 
/*    */   public Thread newThread(Runnable runnable)
/*    */   {
/* 44 */     String name = this.mPrefix + this.mThreadNum.getAndIncrement();
/* 45 */     Thread ret = new Thread(this.mGroup, runnable, name, 0L);
/* 46 */     ret.setDaemon(this.mDaemo);
/* 47 */     return ret;
/*    */   }
/*    */ 
/*    */   public ThreadGroup getThreadGroup()
/*    */   {
/* 52 */     return this.mGroup;
/*    */   }
/*    */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.NamedThreadFactory
 * JD-Core Version:    0.6.0
 */