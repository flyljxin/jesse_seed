package com.wu.common.threadpool;

import java.util.concurrent.Executor;

public abstract interface ThreadPool
{
  public abstract Executor getExecutor();

  public abstract void execute(Runnable paramRunnable);
}

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.ThreadPool
 * JD-Core Version:    0.6.0
 */