/*     */ package com.wu.common.threadpool.fixed;
/*     */ 
/*     */ import com.wu.common.threadpool.AbortPolicyWithReport;
/*     */ import com.wu.common.threadpool.NamedThreadFactory;
/*     */ import com.wu.common.threadpool.ThreadPool;

/*     */ import java.util.concurrent.BlockingQueue;
/*     */ import java.util.concurrent.Executor;
/*     */ import java.util.concurrent.LinkedBlockingQueue;
/*     */ import java.util.concurrent.RejectedExecutionHandler;
/*     */ import java.util.concurrent.SynchronousQueue;
/*     */ import java.util.concurrent.ThreadFactory;
/*     */ import java.util.concurrent.ThreadPoolExecutor;
/*     */ import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.InitializingBean;
/*     */ 
/*     */ public class FixedThreadPool
/*     */   implements ThreadPool, InitializingBean
/*     */ {
/*  28 */   private static Logger logger = Logger.getLogger(FixedThreadPool.class);
/*     */ 
/*  30 */   private String name = "WU";
/*     */ 
/*  32 */   private int threads = 200;
/*     */ 
/*  34 */   private int queues = 0;
/*     */ 
/*  36 */   private RejectedExecutionHandler rejectedHandler = new AbortPolicyWithReport(this.name);
/*     */ 
/*  38 */   private ThreadFactory threadFactory = new NamedThreadFactory(this.name, true);
/*     */   private Executor executor;
/*     */ 
/*     */   public void initialize()
/*     */   {
/*  44 */     this.executor = new ThreadPoolExecutor(this.threads, this.threads, 0L, TimeUnit.MILLISECONDS, (BlockingQueue)(this.queues < 0 ? new LinkedBlockingQueue() : this.queues == 0 ? new SynchronousQueue() : new LinkedBlockingQueue(this.queues)), this.threadFactory, this.rejectedHandler);
/*     */   }
/*     */ 
/*     */   public Executor getExecutor()
/*     */   {
/*  54 */     return this.executor;
/*     */   }
/*     */ 
/*     */   public void execute(Runnable command)
/*     */   {
/*  59 */     this.executor.execute(command);
/*     */   }
/*     */ 
/*     */   public void afterPropertiesSet() throws Exception
/*     */   {
/*  64 */     initialize();
/*     */   }
/*     */ 
/*     */   public String getName() {
/*  68 */     return this.name;
/*     */   }
/*     */ 
/*     */   public void setName(String name) {
/*  72 */     this.name = name;
/*     */   }
/*     */ 
/*     */   public int getThreads() {
/*  76 */     return this.threads;
/*     */   }
/*     */ 
/*     */   public void setThreads(int threads) {
/*  80 */     this.threads = threads;
/*     */   }
/*     */ 
/*     */   public int getQueues() {
/*  84 */     return this.queues;
/*     */   }
/*     */ 
/*     */   public void setQueues(int queues) {
/*  88 */     this.queues = queues;
/*     */   }
/*     */ 
/*     */   public RejectedExecutionHandler getRejectedHandler() {
/*  92 */     return this.rejectedHandler;
/*     */   }
/*     */ 
/*     */   public void setRejectedHandler(RejectedExecutionHandler rejectedHandler) {
/*  96 */     this.rejectedHandler = rejectedHandler;
/*     */   }
/*     */ 
/*     */   public ThreadFactory getThreadFactory() {
/* 100 */     return this.threadFactory;
/*     */   }
/*     */ 
/*     */   public void setThreadFactory(ThreadFactory threadFactory) {
/* 104 */     this.threadFactory = threadFactory;
/*     */   }
/*     */ 
/*     */   public void setExecutor(Executor executor) {
/* 108 */     this.executor = executor;
/*     */   }
/*     */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.fixed.FixedThreadPool
 * JD-Core Version:    0.6.0
 */