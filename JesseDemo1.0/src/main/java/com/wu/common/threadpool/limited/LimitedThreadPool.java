/*     */ package com.wu.common.threadpool.limited;
/*     */ 
/*     */ import com.wu.common.threadpool.AbortPolicyWithReport;
/*     */ import com.wu.common.threadpool.NamedThreadFactory;
/*     */ import com.wu.common.threadpool.ThreadPool;

/*     */ import java.util.concurrent.BlockingQueue;
/*     */ import java.util.concurrent.Executor;
/*     */ import java.util.concurrent.LinkedBlockingQueue;
/*     */ import java.util.concurrent.RejectedExecutionHandler;
/*     */ import java.util.concurrent.SynchronousQueue;
/*     */ import java.util.concurrent.ThreadFactory;
/*     */ import java.util.concurrent.ThreadPoolExecutor;
/*     */ import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
/*     */ import org.springframework.beans.factory.InitializingBean;
/*     */ 
/*     */ public class LimitedThreadPool
/*     */   implements ThreadPool, InitializingBean
/*     */ {
/*  28 */   private static Logger logger = Logger.getLogger(LimitedThreadPool.class);
/*     */ 
/*  30 */   private String name = "WU";
/*     */ 
/*  32 */   private int cores = 0;
/*     */ 
/*  34 */   private int threads = 200;
/*     */ 
/*  36 */   private int queues = 0;
/*     */ 
/*  38 */   private RejectedExecutionHandler rejectedHandler = new AbortPolicyWithReport(this.name);
/*     */ 
/*  40 */   private ThreadFactory threadFactory = new NamedThreadFactory(this.name, true);
/*     */   private Executor executor;
/*     */ 
/*     */   public void initialize()
/*     */   {
/*  46 */     this.executor = new ThreadPoolExecutor(this.cores, this.threads, 9223372036854775807L, TimeUnit.MILLISECONDS, (BlockingQueue)(this.queues < 0 ? new LinkedBlockingQueue() : this.queues == 0 ? new SynchronousQueue() : new LinkedBlockingQueue(this.queues)), this.threadFactory, this.rejectedHandler);
/*     */   }
/*     */ 
/*     */   public void afterPropertiesSet()
/*     */     throws Exception
/*     */   {
/*  56 */     initialize();
/*     */   }
/*     */ 
/*     */   public Executor getExecutor()
/*     */   {
/*  61 */     return this.executor;
/*     */   }
/*     */ 
/*     */   public void execute(Runnable command)
/*     */   {
/*  66 */     this.executor.execute(command);
/*     */   }
/*     */ 
/*     */   public String getName() {
/*  70 */     return this.name;
/*     */   }
/*     */ 
/*     */   public void setName(String name) {
/*  74 */     this.name = name;
/*     */   }
/*     */ 
/*     */   public int getCores() {
/*  78 */     return this.cores;
/*     */   }
/*     */ 
/*     */   public void setCores(int cores) {
/*  82 */     this.cores = cores;
/*     */   }
/*     */ 
/*     */   public int getThreads() {
/*  86 */     return this.threads;
/*     */   }
/*     */ 
/*     */   public void setThreads(int threads) {
/*  90 */     this.threads = threads;
/*     */   }
/*     */ 
/*     */   public int getQueues() {
/*  94 */     return this.queues;
/*     */   }
/*     */ 
/*     */   public void setQueues(int queues) {
/*  98 */     this.queues = queues;
/*     */   }
/*     */ 
/*     */   public RejectedExecutionHandler getRejectedHandler() {
/* 102 */     return this.rejectedHandler;
/*     */   }
/*     */ 
/*     */   public void setRejectedHandler(RejectedExecutionHandler rejectedHandler) {
/* 106 */     this.rejectedHandler = rejectedHandler;
/*     */   }
/*     */ 
/*     */   public ThreadFactory getThreadFactory() {
/* 110 */     return this.threadFactory;
/*     */   }
/*     */ 
/*     */   public void setThreadFactory(ThreadFactory threadFactory) {
/* 114 */     this.threadFactory = threadFactory;
/*     */   }
/*     */ 
/*     */   public void setExecutor(Executor executor) {
/* 118 */     this.executor = executor;
/*     */   }
/*     */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.threadpool.limited.LimitedThreadPool
 * JD-Core Version:    0.6.0
 */