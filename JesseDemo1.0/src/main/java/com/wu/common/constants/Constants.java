package com.wu.common.constants;

public class Constants
{
  public static final String COLON_SIGN = ":";
  public static final String URL_SIGN = "://";
  public static final String DEFAULT_THREAD_NAME = "WU";
  public static final int DEFAULT_CORE_THREADS = 0;
  public static final int DEFAULT_THREADS = 200;
  public static final int DEFAULT_QUEUES = 0;
  public static final int DEFAULT_ALIVE = 60000;
}

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.constants.Constants
 * JD-Core Version:    0.6.0
 */