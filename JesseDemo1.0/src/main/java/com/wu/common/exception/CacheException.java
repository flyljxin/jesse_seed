/*    */ package com.wu.common.exception;
/*    */ 
/*    */ public class CacheException extends RuntimeException
/*    */ {
/*    */   private static final long serialVersionUID = -2045559468370767395L;
/*    */ 
/*    */   public CacheException()
/*    */   {
/*    */   }
/*    */ 
/*    */   public CacheException(String message)
/*    */   {
/* 28 */     super(message);
/*    */   }
/*    */ 
/*    */   public CacheException(Throwable cause)
/*    */   {
/* 36 */     super(cause);
/*    */   }
/*    */ 
/*    */   public CacheException(String message, Throwable cause)
/*    */   {
/* 45 */     super(message, cause);
/*    */   }
/*    */ 
/*    */   public CacheException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
/*    */   {
/* 56 */     super(message, cause, enableSuppression, writableStackTrace);
/*    */   }
/*    */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.exception.CacheException
 * JD-Core Version:    0.6.0
 */