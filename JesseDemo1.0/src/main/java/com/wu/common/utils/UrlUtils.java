/*    */ package com.wu.common.utils;
/*    */ 
/*    */ import java.io.UnsupportedEncodingException;
/*    */ import java.net.URLDecoder;
/*    */ import java.net.URLEncoder;
/*    */ import org.apache.commons.lang3.StringUtils;
/*    */ 
/*    */ public class UrlUtils
/*    */ {
/*    */   public static final String DEFAULT_ENCODING = "UTF-8";
/*    */ 
/*    */   public static String encode(String value)
/*    */   {
/* 32 */     if (StringUtils.isEmpty(value))
/* 33 */       return "";
/*    */     try
/*    */     {
/* 36 */       return URLEncoder.encode(value, "UTF-8"); } catch (UnsupportedEncodingException e) {
	/* 38 */     throw new RuntimeException(e.getMessage(), e);
/*    */     }
/*    */   }
/*    */ 
/*    */   public static String decode(String value)
/*    */   {
/* 48 */     if (StringUtils.isEmpty(value))
/* 49 */       return "";
/*    */     try
/*    */     {
/* 52 */       return URLDecoder.decode(value, "UTF-8"); } catch (UnsupportedEncodingException e) {
	/* 54 */     throw new RuntimeException(e.getMessage(), e);
/*    */     }
/*    */   }
/*    */ }
