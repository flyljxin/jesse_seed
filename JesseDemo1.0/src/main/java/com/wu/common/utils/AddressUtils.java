/*     */ package com.wu.common.utils;
/*     */ 
/*     */ import java.net.InetAddress;
/*     */ import java.net.NetworkInterface;
/*     */ import java.util.Enumeration;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ 
/*     */ public class AddressUtils
/*     */ {
/*     */   private static final String LOCALHOST_IP = "127.0.0.1";
/*     */   private static final String EMPTY_IP = "0.0.0.0";
/*  20 */   private static final Pattern IP_PATTERN = Pattern.compile("[0-9]{1,3}(\\.[0-9]{1,3}){3,}");
/*     */ 
/*     */   public static InetAddress getHostAddress()
/*     */   {
/*  33 */     InetAddress localAddress = null;
/*     */     try
/*     */     {
/*  37 */       localAddress = InetAddress.getLocalHost();
/*     */ 
/*  39 */       if (isValidAddress(localAddress)) {
/*  40 */         return localAddress;
/*     */       }
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/*     */     }
/*     */ 
/*     */     try
/*     */     {
/*  49 */       Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
/*     */ 
/*  51 */       if (interfaces != null)
/*     */       {
/*  53 */         while (interfaces.hasMoreElements())
/*     */         {
/*     */           try
/*     */           {
/*  57 */             NetworkInterface network = (NetworkInterface)interfaces.nextElement();
/*  58 */             Enumeration addresses = network.getInetAddresses();
/*     */ 
/*  60 */             if (addresses != null)
/*     */             {
/*  62 */               while (addresses.hasMoreElements())
/*     */               {
/*     */                 try
/*     */                 {
/*  66 */                   InetAddress address = (InetAddress)addresses.nextElement();
/*     */ 
/*  68 */                   if (isValidAddress(address))
/*     */                   {
/*  70 */                     return address;
/*     */                   }
/*     */                 }
/*     */                 catch (Throwable e)
/*     */                 {
/*     */                 }
/*     */               }
/*     */             }
/*     */           }
/*     */           catch (Throwable e)
/*     */           {
/*     */           }
/*     */ 
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/*     */     }
/*     */ 
/*  92 */     return localAddress;
/*     */   }
/*     */ 
/*     */   public static String getHostIP()
/*     */   {
/*     */     try
/*     */     {
/*  99 */       return getHostAddress().getHostAddress();
/*     */     }
/*     */     catch (Exception e) {
/*     */     }
/* 103 */     return "127.0.0.1";
/*     */   }
/*     */ 
/*     */   public static boolean isValidAddress(InetAddress address)
/*     */   {
/* 109 */     if ((address == null) || (address.isLoopbackAddress()))
/* 110 */       return false;
/* 111 */     String name = address.getHostAddress();
/* 112 */     return (name != null) && (!"0.0.0.0".equals(name)) && (!"127.0.0.1".equals(name)) && (IP_PATTERN.matcher(name).matches());
/*     */   }
/*     */ 
/*     */   public static boolean isValidAddress(String address)
/*     */   {
/* 117 */     return (address != null) && (!"0.0.0.0".equals(address)) && (!"127.0.0.1".equals(address)) && (IP_PATTERN.matcher(address).matches());
/*     */   }
/*     */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.utils.AddressUtils
 * JD-Core Version:    0.6.0
 */