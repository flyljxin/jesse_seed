/*    */ package com.wu.common.utils;
/*    */ 
/*    */ import java.lang.management.ManagementFactory;
/*    */ import java.lang.management.RuntimeMXBean;
/*    */ import org.apache.commons.lang3.StringUtils;
/*    */ 
/*    */ public class SystemUtils
/*    */ {
/*    */   public static String getPid()
/*    */   {
/* 24 */     String[] pidInfo = StringUtils.split(ManagementFactory.getRuntimeMXBean().getName(), '@');
/*    */ 
/* 26 */     return pidInfo[0];
/*    */   }
/*    */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.utils.SystemUtils
 * JD-Core Version:    0.6.0
 */