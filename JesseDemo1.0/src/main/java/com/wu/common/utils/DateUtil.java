package com.wu.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{
  public static Date getNextDate(Date date, int delay)
  {
    Calendar calendar = Calendar.getInstance();
    if (date != null)
      calendar.setTime(date);
    calendar.add(5, delay);
    return calendar.getTime();
  }

  public static String getFormatDate(Date date, String format)
  {
    DateFormat df = new SimpleDateFormat(format);
    return df.format(date);
  }

  public static String getFormatDate(String format)
  {
    return getFormatDate(new Date(), format);
  }

  public static String getNextDateString(Date date, int delay, String format)
  {
    return getFormatDate(getNextDate(date, delay), format);
  }

  public static String getNextDateString(int delay, String format)
  {
    return getFormatDate(getNextDate(null, delay), format);
  }
}
