/*    */ package com.wu.common.utils;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.net.URL;
/*    */ import java.security.CodeSource;
/*    */ import java.security.ProtectionDomain;
/*    */ 
/*    */ public class AppUtils
/*    */ {
/*    */   public static String getAppPath()
/*    */   {
/* 24 */     File jarPath = new File(UrlUtils.decode(AppUtils.class.getProtectionDomain().getCodeSource().getLocation().getFile()));
/* 25 */     return jarPath.getParentFile().getParentFile().getAbsolutePath();
/*    */   }
/*    */ }

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.utils.AppUtils
 * JD-Core Version:    0.6.0
 */