package com.wu.common.cache.redis;

import com.wu.common.cache.ICache;
import com.wu.common.exception.CacheException;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class RedisFactory
{
  private static Logger logger = Logger.getLogger(RedisFactory.class);

  private static final Map<String, ICache> cacherPool = new ConcurrentHashMap();

  public static boolean lock(String key, int timeoutSeconds, ICache cacher)
  {
    if ((cacher.getOriginalCacher() instanceof JedisPool)) {
      return lockWithNonCluster(key, timeoutSeconds, cacher);
    }
    return lockWithCluster(key, timeoutSeconds, cacher);
  }

  private static boolean lockWithNonCluster(String key, int timeoutSeconds, ICache cacher)
  {
    JedisPool jedisPool = getJedisPool(cacher);
    if (logger.isDebugEnabled()) {
      logger.debug("lock the key=" + key);
    }

    Jedis jedis = jedisPool.getResource();
    try {
      Long i = jedis.setnx(key, key);
      if (i.longValue() == 1L) {
        jedis.expire(key, timeoutSeconds);
        if (logger.isDebugEnabled()) {
          logger.debug("get lock, key: " + key + " , expire in " + timeoutSeconds + " seconds.");
        }
//        i = 1;
        return true;
      }
//      int i = 0;
      return false;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      jedisPool.returnResource(jedis); }// throw localObject;
  }

  private static boolean lockWithCluster(String key, int timeoutSeconds, ICache cacher)
  {
    if (logger.isDebugEnabled())
      logger.debug("lock the key=" + key);
    try
    {
      JedisCluster jedisCluster = (JedisCluster)cacher.getOriginalCacher();
      Long i = jedisCluster.setnx(key, key);
      if (i.longValue() == 1L) {
        jedisCluster.expire(key, timeoutSeconds);
        if (logger.isDebugEnabled()) {
          logger.debug("get lock, key: " + key + " , expire in " + timeoutSeconds + " seconds.");
        }
        return true;
      }
      return false; } catch (Exception e) {
    }
    Throwable e = null;
	throw new CacheException("Redis operation error.", e);
  }

  public static void unLock(String key, ICache cacher)
  {
    delKey(key, cacher);
  }

  public static long delKey(String key, ICache cacher)
  {
    if ((cacher.getOriginalCacher() instanceof JedisPool)) {
      return delKeyWihNonCluster(key, cacher);
    }
    return delKeyWihCluster(key, cacher);
  }

  private static long delKeyWihNonCluster(String key, ICache cacher)
  {
    JedisPool jedisPool = getJedisPool(cacher);
    if (logger.isDebugEnabled()) {
      logger.debug(" delete the number with key=" + key);
    }
    Jedis jedis = jedisPool.getResource();
    try {
      long l = jedis.del(key).longValue();
      return l;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      jedisPool.returnResource(jedis); } //throw localObject;
  }

  private static long delKeyWihCluster(String key, ICache cacher)
  {
    if (logger.isDebugEnabled())
      logger.debug(" delete the number with key=" + key);
    try
    {
      JedisCluster jedisCluster = (JedisCluster)cacher.getOriginalCacher();
      return jedisCluster.del(key).longValue(); } catch (Exception e) {
    }
    throw new CacheException("Redis operation error.");
  }

  public static long incr(String key, ICache cacher)
  {
    if ((cacher.getOriginalCacher() instanceof JedisPool)) {
      return incrWithNonCluster(key, cacher);
    }
    return incrWithCluster(key, cacher);
  }

  private static long incrWithNonCluster(String key, ICache cacher)
  {
    JedisPool jedisPool = getJedisPool(cacher);
    if (logger.isDebugEnabled()) {
      logger.debug(" increase the number with key=" + key);
    }
    Jedis jedis = jedisPool.getResource();
    try {
      long l = jedis.incr(key).longValue();
      return l;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      jedisPool.returnResource(jedis); }// throw localObject;
  }

  private static long incrWithCluster(String key, ICache cacher)
  {
    if (logger.isDebugEnabled())
      logger.debug(" increase the number with key=" + key);
    try
    {
      JedisCluster jedisCluster = (JedisCluster)cacher.getOriginalCacher();
      return jedisCluster.incr(key).longValue(); } catch (Exception e) {
    } 
    throw new CacheException("Redis operation error.");
  }

  public static long incr(String key, int timeoutSeconds, ICache cacher)
  {
    if ((cacher.getOriginalCacher() instanceof JedisPool)) {
      return incrWithNonCluster(key, timeoutSeconds, cacher);
    }
    return incrWithCluster(key, timeoutSeconds, cacher);
  }

  private static long incrWithNonCluster(String key, int timeoutSeconds, ICache cacher)
  {
    JedisPool jedisPool = getJedisPool(cacher);
    if (logger.isDebugEnabled()) {
      logger.debug(" increase the number with key=" + key);
    }
    Jedis jedis = jedisPool.getResource();
    try {
      long times = jedis.incr(key).longValue();
      jedis.expire(key.getBytes(), timeoutSeconds);
      long l1 = times;
      return l1;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      jedisPool.returnResource(jedis); } //throw localObject;
  }

  private static long incrWithCluster(String key, int timeoutSeconds, ICache cacher)
  {
    if (logger.isDebugEnabled())
      logger.debug(" increase the number with key=" + key);
    try
    {
      JedisCluster jedisCluster = (JedisCluster)cacher.getOriginalCacher();
      long times = jedisCluster.incr(key).longValue();
      jedisCluster.expire(key.getBytes(), timeoutSeconds);
      return times; } catch (Exception e) {
    }
    throw new CacheException("Redis operation error.");
  }

  public static long decr(String key, ICache cacher)
  {
    if ((cacher.getOriginalCacher() instanceof JedisPool)) {
      return decrWithNonCluster(key, cacher);
    }
    return decrWithCluster(key, cacher);
  }

  private static long decrWithNonCluster(String key, ICache cacher)
  {
    JedisPool jedisPool = getJedisPool(cacher);
    if (logger.isDebugEnabled()) {
      logger.debug("decrease the number with key=" + key);
    }
    Jedis jedis = jedisPool.getResource();
    try {
      long l = jedis.decr(key).longValue();
      return l;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      jedisPool.returnResource(jedis); } //throw localObject;
  }

  private static long decrWithCluster(String key, ICache cacher)
  {
    if (logger.isDebugEnabled())
      logger.debug("decrease the number with key=" + key);
    try
    {
      JedisCluster jedisCluster = (JedisCluster)cacher.getOriginalCacher();
      return jedisCluster.decr(key).longValue(); } catch (Exception e) {
    }
    throw new CacheException("Redis operation error.");
  }

  public static void addCacher(String key, ICache cacher)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("add cacher with the key=" + key);
    }
    cacherPool.put(key, cacher);
  }

  private static JedisPool getJedisPool(ICache cacher) {
    return (JedisPool)cacher.getOriginalCacher();
  }

  public static Object getJedisPool(String key) {
    return getCacher(key).getOriginalCacher();
  }

  public static ICache getCacher(String key) {
    if (logger.isDebugEnabled()) {
      logger.debug("get cacher with the key=" + key);
    }
    return (ICache)cacherPool.get(key);
  }

  public static Map<String, ICache> getCachers() {
    return cacherPool;
  }

  public static void destroy() {
    for (Map.Entry entry : cacherPool.entrySet())
      if ((((ICache)entry.getValue()).getOriginalCacher() instanceof JedisPool)) {
        JedisPool jedisPool = (JedisPool)((ICache)entry.getValue()).getOriginalCacher();
        try {
          jedisPool.destroy();
        } catch (Throwable t) {
          logger.warn("Failed to destroy the redis registry client. registry: " + (String)entry.getKey() + ", cause: " + t.getMessage(), t);
        }
      }
      else {
        JedisCluster jedisCluster = (JedisCluster)((ICache)entry.getValue()).getOriginalCacher();
        try {
          jedisCluster.close();
        } catch (Exception e) {
          logger.warn("Failed to destroy the redis registry client. registry: " + (String)entry.getKey() + ", cause: " + e.getMessage(), e);
        }
      }
  }

  public static void destroy(String key)
  {
    if ((getCacher(key).getOriginalCacher() instanceof JedisPool)) {
      JedisPool jedisPool = (JedisPool)getCacher(key).getOriginalCacher();
      try {
        jedisPool.destroy();
      } catch (Throwable t) {
        logger.warn("Failed to destroy the redis registry client. registry: " + key + ", cause: " + t.getMessage(), t);
      }
    }
    else
    {
      JedisCluster jedisCluster = (JedisCluster)getCacher(key).getOriginalCacher();
      try {
        jedisCluster.close();
      } catch (Exception e) {
        logger.warn("Failed to destroy the redis registry client. registry: " + key + ", cause: " + e.getMessage(), e);
      }
    }
  }

  public static GenericObjectPoolConfig generatePoolConfig(RedisConfig redisInfo)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("The redis config=" + redisInfo);
    }
    GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
    genericObjectPoolConfig.setTestOnBorrow(redisInfo.isTestOnBorrow());
    genericObjectPoolConfig.setTestOnReturn(redisInfo.isTestOnReturn());
    genericObjectPoolConfig.setTestWhileIdle(redisInfo.isTestWhileIdle());
    if (redisInfo.getMaxIdle() > 0)
      genericObjectPoolConfig.setMaxIdle(redisInfo.getMaxIdle());
    if (redisInfo.getMinIdle() > 0)
      genericObjectPoolConfig.setMinIdle(redisInfo.getMinIdle());
    if (redisInfo.getMaxActive() > 0)
      genericObjectPoolConfig.setMaxTotal(redisInfo.getMaxActive());
    if (redisInfo.getMaxWait() > 0)
      genericObjectPoolConfig.setMaxWaitMillis(redisInfo.getMaxWait());
    if (redisInfo.getNumTestsPerEvictionRun() > 0)
      genericObjectPoolConfig.setNumTestsPerEvictionRun(redisInfo.getNumTestsPerEvictionRun());
    if (redisInfo.getTimeBetweenEvictionRunsMillis() > 0)
      genericObjectPoolConfig.setTimeBetweenEvictionRunsMillis(redisInfo.getTimeBetweenEvictionRunsMillis());
    if (redisInfo.getMinEvictableIdleTimeMillis() > 0) {
      genericObjectPoolConfig.setMinEvictableIdleTimeMillis(redisInfo.getMinEvictableIdleTimeMillis());
    }
    return genericObjectPoolConfig;
  }
}
