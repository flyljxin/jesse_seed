package com.wu.common.cache;

import java.util.List;

public abstract interface ICache
{
  public abstract Object get(String paramString);

  public abstract void put(String paramString, Object paramObject);

  public abstract void put(String paramString, Object paramObject, int paramInt);

  public abstract void clear(String paramString);

  public abstract void clear(List<String> paramList);

  public abstract Object getOriginalCacher();
}

/* Location:           C:\Users\0160093\Desktop\wu-service-common-1.0.3.jar
 * Qualified Name:     com.wu.common.cache.ICache
 * JD-Core Version:    0.6.0
 */