package com.wu.common.cache.redis;

import com.wu.common.cache.ICache;
import com.wu.common.exception.CacheException;
import com.wu.common.utils.SerializeUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

public class RedisClusterCacher
  implements ICache, InitializingBean
{
  protected static Logger logger = Logger.getLogger(RedisClusterCacher.class);
  private JedisCluster jedisCluster;
  private RedisConfig redisConfig;

  public Object get(String id)
  {
    if (logger.isDebugEnabled())
      logger.debug("Get the obj with id=" + id);
    try
    {
      byte[] data = (byte[])this.jedisCluster.get(id.getBytes());
      if (data == null) return null;
      return SerializeUtil.unserialize(data); } catch (Exception e) {
    }
    Throwable e = null;
	throw new CacheException("Redis operation error.", e);
  }

  public void put(String id, Object obj)
  {
    if (logger.isDebugEnabled())
      logger.debug("Save the Obj with id=" + id);
    try
    {
      this.jedisCluster.set(id.getBytes(), SerializeUtil.serialize(obj));
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void put(String id, Object obj, int timeoutSeconds)
  {
    if (logger.isDebugEnabled())
      logger.debug("Save the Obj with id=" + id + ",timeout=" + timeoutSeconds);
    try
    {
      this.jedisCluster.set(id.getBytes(), SerializeUtil.serialize(obj));
      Long timeoutResult = this.jedisCluster.expire(id.getBytes(), timeoutSeconds);
      if (logger.isDebugEnabled())
        logger.debug("The timeout setting result " + timeoutResult);
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void clear(String id)
  {
    if (logger.isDebugEnabled())
      logger.debug("Del the Obj with id=" + id);
    try
    {
      this.jedisCluster.del(id.getBytes());
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void clear(List<String> ids)
  {
    if (logger.isDebugEnabled())
      logger.debug("Del the Objs with ids=" + ids);
    try
    {
      for (String id : ids)
        this.jedisCluster.del(id.getBytes());
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public RedisConfig getRedisConfig() {
    return this.redisConfig;
  }

  public void setRedisConfig(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
  }

  public void afterPropertiesSet() throws Exception
  {
    try {
      if (logger.isDebugEnabled()) {
        logger.debug("The redis host=" + this.redisConfig.getAddresses());
      }
      if ((StringUtils.isEmpty(this.redisConfig.getAddresses())) || (this.redisConfig.getPort() == 0))
      {
        logger.error("The redis host or port is null.");
        throw new CacheException("The redis host or port is null.");
      }
      String redisServerKey = this.redisConfig.getRedisServerKey();
      if (RedisFactory.getCacher(redisServerKey) == null)
      {
        GenericObjectPoolConfig genericObjectPoolConfig = RedisFactory.generatePoolConfig(this.redisConfig);

        String[] ipAndPort = StringUtils.split(this.redisConfig.getAddresses(), ";");
        Set hostAndPorts = new HashSet();
        for (String hostAndPort : ipAndPort)
          if (!StringUtils.isEmpty(hostAndPort)) {
            String[] datas = StringUtils.split(hostAndPort, ":");
            hostAndPorts.add(new HostAndPort(datas[0], Integer.parseInt(datas[1])));
          }
        this.jedisCluster = new JedisCluster(hostAndPorts, this.redisConfig.getTimeout(), genericObjectPoolConfig);
        RedisFactory.addCacher(redisServerKey, this);
      } else {
        this.jedisCluster = ((JedisCluster)RedisFactory.getCacher(redisServerKey).getOriginalCacher());
      }
    } catch (Exception e) {
      logger.error("Redis init error.", e);
      throw new CacheException("Redis init error.", e);
    }
  }

  public JedisCluster getJedisCluster()
  {
    return this.jedisCluster;
  }

  public Object getOriginalCacher()
  {
    return getJedisCluster();
  }
}
