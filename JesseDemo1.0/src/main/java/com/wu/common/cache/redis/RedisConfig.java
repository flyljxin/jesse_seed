package com.wu.common.cache.redis;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;

public class RedisConfig
  implements InitializingBean
{
  private String host;
  private int port;
  private String addresses;
  private boolean isCluster = false;
  private String password;
  private int maxIdle;
  private int maxTotal;
  private int minIdle;
  private boolean testOnBorrow;
  private boolean usePool;
  private int timeout;
  private boolean testOnReturn;
  private boolean testWhileIdle;
  private int maxActive;
  private int maxWait;
  private int numTestsPerEvictionRun;
  private int timeBetweenEvictionRunsMillis;
  private int minEvictableIdleTimeMillis;

  public String getAddresses()
  {
    return this.addresses;
  }

  public void setAddresses(String addresses) {
    this.addresses = addresses;
    if ((addresses != null) && (!"".equals(addresses.trim())))
      setCluster(true);
  }

  public boolean isCluster()
  {
    return this.isCluster;
  }

  public void setCluster(boolean isCluster) {
    this.isCluster = isCluster;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getMaxIdle() {
    return this.maxIdle;
  }

  public void setMaxIdle(int maxIdle) {
    this.maxIdle = maxIdle;
  }

  public int getMaxTotal() {
    return this.maxTotal;
  }

  public void setMaxTotal(int maxTotal) {
    this.maxTotal = maxTotal;
  }

  public int getMinIdle() {
    return this.minIdle;
  }

  public void setMinIdle(int minIdle) {
    this.minIdle = minIdle;
  }

  public boolean isTestOnBorrow() {
    return this.testOnBorrow;
  }

  public void setTestOnBorrow(boolean testOnBorrow) {
    this.testOnBorrow = testOnBorrow;
  }

  public boolean isUsePool() {
    return this.usePool;
  }

  public void setUsePool(boolean usePool) {
    this.usePool = usePool;
  }

  public int getTimeout() {
    return this.timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  public boolean isTestOnReturn() {
    return this.testOnReturn;
  }

  public void setTestOnReturn(boolean testOnReturn) {
    this.testOnReturn = testOnReturn;
  }

  public boolean isTestWhileIdle() {
    return this.testWhileIdle;
  }

  public void setTestWhileIdle(boolean testWhileIdle) {
    this.testWhileIdle = testWhileIdle;
  }

  public int getMaxActive() {
    return this.maxActive;
  }

  public void setMaxActive(int maxActive) {
    this.maxActive = maxActive;
  }

  public int getMaxWait() {
    return this.maxWait;
  }

  public void setMaxWait(int maxWait) {
    this.maxWait = maxWait;
  }

  public int getNumTestsPerEvictionRun() {
    return this.numTestsPerEvictionRun;
  }

  public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
    this.numTestsPerEvictionRun = numTestsPerEvictionRun;
  }

  public int getTimeBetweenEvictionRunsMillis() {
    return this.timeBetweenEvictionRunsMillis;
  }

  public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
    this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
  }

  public int getMinEvictableIdleTimeMillis() {
    return this.minEvictableIdleTimeMillis;
  }

  public void setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
    this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
  }

  public String getHost() {
    return this.host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public int getPort() {
    return this.port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getRedisServerKey() {
    if (this.isCluster) {
      return this.addresses;
    }
    return getHost().trim() + ":" + getPort();
  }

  public void afterPropertiesSet() throws Exception
  {
    if ((this.host.indexOf(";") > 0) && (StringUtils.isEmpty(getAddresses())))
      setAddresses(this.host);
  }

  public String toString()
  {
    return "RedisConfig [host=" + this.host + ", port=" + this.port + ", addresses=" + this.addresses + ", isCluster=" + this.isCluster + ", password=" + this.password + ", maxIdle=" + this.maxIdle + ", maxTotal=" + this.maxTotal + ", minIdle=" + this.minIdle + ", testOnBorrow=" + this.testOnBorrow + ", usePool=" + this.usePool + ", timeout=" + this.timeout + ", testOnReturn=" + this.testOnReturn + ", testWhileIdle=" + this.testWhileIdle + ", maxActive=" + this.maxActive + ", maxWait=" + this.maxWait + ", numTestsPerEvictionRun=" + this.numTestsPerEvictionRun + ", timeBetweenEvictionRunsMillis=" + this.timeBetweenEvictionRunsMillis + ", minEvictableIdleTimeMillis=" + this.minEvictableIdleTimeMillis + "]";
  }
}
