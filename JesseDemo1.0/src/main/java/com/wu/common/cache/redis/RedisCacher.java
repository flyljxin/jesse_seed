package com.wu.common.cache.redis;

import com.wu.common.cache.ICache;
import com.wu.common.exception.CacheException;
import com.wu.common.utils.SerializeUtil;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisCacher
  implements ICache, InitializingBean
{
	private static final org.apache.log4j.Logger logger = Logger.getLogger(RedisCacher.class);

  private JedisPool jedisPool;
  private RedisConfig redisConfig;

  public Object get(String id)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Get the obj with id=" + id);
    }
    Jedis jedis = this.jedisPool.getResource();
    try {
      byte[] data = (byte[])jedis.get(id.getBytes());
      if (data == null) { Object localObject1 = null;
        return localObject1;
      }
      Object localObject1 = SerializeUtil.unserialize(data);
      return localObject1;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.jedisPool.returnResource(jedis); } 
  }

  public void put(String id, Object obj)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Save the Obj with id=" + id);
    }
    Jedis jedis = this.jedisPool.getResource();
    try {
      jedis.set(id.getBytes(), SerializeUtil.serialize(obj));
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.jedisPool.returnResource(jedis);
    }
  }

  public void put(String id, Object obj, int timeoutSeconds)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Save the Obj with id=" + id + ",timeout=" + timeoutSeconds);
    }
    Jedis jedis = this.jedisPool.getResource();
    try {
      jedis.set(id.getBytes(), SerializeUtil.serialize(obj));
      Long timeoutResult = jedis.expire(id.getBytes(), timeoutSeconds);
      if (logger.isDebugEnabled())
        logger.debug("The timeout setting result " + timeoutResult);
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.jedisPool.returnResource(jedis);
    }
  }

  public void clear(String id)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Del the Obj with id=" + id);
    }
    Jedis jedis = this.jedisPool.getResource();
    try {
      jedis.del(id.getBytes());
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.jedisPool.returnResource(jedis);
    }
  }

  public void clear(List<String> ids)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Del the Objs with ids=" + ids);
    }
    Jedis jedis = this.jedisPool.getResource();
    try {
      for (String id : ids)
        jedis.del(id.getBytes());
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.jedisPool.returnResource(jedis);
    }
  }

  public JedisPool getJedisPool()
  {
    return this.jedisPool;
  }

  public void setJedisPool(JedisPool jedisPool) {
    this.jedisPool = jedisPool;
  }

  public RedisConfig getRedisConfig() {
    return this.redisConfig;
  }

  public void setRedisConfig(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
  }

  public void afterPropertiesSet() throws Exception
  {
    try {
      if (logger.isDebugEnabled()) {
        logger.debug("The redis host=" + this.redisConfig.getHost() + ",port=" + this.redisConfig.getPort());
      }

      if ((StringUtils.isEmpty(this.redisConfig.getHost())) || (this.redisConfig.getPort() == 0))
      {
        logger.error("The redis host or port is null.");
        throw new CacheException("The redis host or port is null.");
      }
      String redisServerKey = this.redisConfig.getRedisServerKey();
      if (RedisFactory.getCacher(redisServerKey) == null)
      {
        GenericObjectPoolConfig genericObjectPoolConfig = RedisFactory.generatePoolConfig(this.redisConfig);

        JedisPool jedisPool = new JedisPool(genericObjectPoolConfig, this.redisConfig.getHost().trim(), this.redisConfig.getPort(), this.redisConfig.getTimeout(), null);

        this.jedisPool = jedisPool;

        RedisFactory.addCacher(redisServerKey, this);
      } else {
        this.jedisPool = ((JedisPool)RedisFactory.getCacher(redisServerKey).getOriginalCacher());
      }
    } catch (Exception e) {
      logger.error("Redis init error.", e);
      throw new CacheException("Redis init error.", e);
    }
  }

  public Object getOriginalCacher()
  {
    return getJedisPool();
  }
}