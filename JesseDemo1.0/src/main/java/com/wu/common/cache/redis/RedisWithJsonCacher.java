package com.wu.common.cache.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.wu.common.cache.ICache;
import com.wu.common.exception.CacheException;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.xerial.snappy.Snappy;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

public class RedisWithJsonCacher
  implements ICache, InitializingBean
{
  protected static Logger logger = Logger.getLogger(RedisWithJsonCacher.class);

  private boolean isCluster = false;
  private RedisConfig redisConfig;
  private RedisCacher redisCacher;
  private RedisClusterCacher redisClusterCacher;

  public Object get(String id)
  {
    if (this.isCluster) {
      return getWithCluster(id);
    }
    return getWithNonCluster(id);
  }

  private Object getWithNonCluster(String id)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Get the obj with id=" + id);
    }
    Jedis jedis = this.redisCacher.getJedisPool().getResource();
    try
    {
      byte[] data = (byte[])jedis.get(id.getBytes());

      if (data == null) {
        Object localObject1 = null;
        return localObject1;
      }
      byte[] uncompressed = Snappy.uncompress(data);
      Object t = JSON.parse(uncompressed, new Feature[0]);
      Object localObject2 = t;
      return localObject2;
    }
    catch (Exception e)
    {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.redisCacher.getJedisPool().returnResource(jedis); }// throw localObject3;
  }

  private Object getWithCluster(String id)
  {
    if (logger.isDebugEnabled())
      logger.debug("Get the obj with id=" + id);
    try
    {
      byte[] data = (byte[])this.redisClusterCacher.getJedisCluster().get(id.getBytes());

      if (data == null)
        return null;
      byte[] uncompressed = Snappy.uncompress(data);
      Object t = JSON.parse(uncompressed, new Feature[0]);
      return t; } catch (Exception e) {
    }
    throw new CacheException("Redis operation error.");
  }

  public void put(String id, Object obj)
  {
    if (this.isCluster)
      putWithCluster(id, obj);
    else
      putWithNonCluster(id, obj);
  }

  private void putWithNonCluster(String id, Object obj)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Save the Obj with id=" + id);
    }
    Jedis jedis = this.redisCacher.getJedisPool().getResource();
    try {
      byte[] compressed = Snappy.compress(JSON.toJSONString(obj, new SerializerFeature[] { SerializerFeature.WriteClassName, SerializerFeature.DisableCircularReferenceDetect }).getBytes("UTF-8"));

      jedis.set(id.getBytes(), compressed);
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.redisCacher.getJedisPool().returnResource(jedis);
    }
  }

  private void putWithCluster(String id, Object obj) {
    if (logger.isDebugEnabled())
      logger.debug("Save the Obj with id=" + id);
    try
    {
      byte[] compressed = Snappy.compress(JSON.toJSONString(obj, new SerializerFeature[] { SerializerFeature.WriteClassName, SerializerFeature.DisableCircularReferenceDetect }).getBytes("UTF-8"));

      this.redisClusterCacher.getJedisCluster().set(id.getBytes(), compressed);
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void put(String id, Object obj, int timeoutSeconds)
  {
    if (this.isCluster)
      putWithCluster(id, obj, timeoutSeconds);
    else
      putWithNonCluster(id, obj, timeoutSeconds);
  }

  private void putWithNonCluster(String id, Object obj, int timeoutSeconds)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Save the Obj with id=" + id + ",timeout=" + timeoutSeconds);
    }
    Jedis jedis = this.redisCacher.getJedisPool().getResource();
    try {
      byte[] compressed = Snappy.compress(JSON.toJSONString(obj, new SerializerFeature[] { SerializerFeature.WriteClassName, SerializerFeature.DisableCircularReferenceDetect }).getBytes("UTF-8"));

      jedis.set(id.getBytes(), compressed);
      Long timeoutResult = jedis.expire(id.getBytes(), timeoutSeconds);
      if (logger.isDebugEnabled())
        logger.debug("The timeout setting result " + timeoutResult);
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.redisCacher.getJedisPool().returnResource(jedis);
    }
  }

  private void putWithCluster(String id, Object obj, int timeoutSeconds) {
    if (logger.isDebugEnabled())
      logger.debug("Save the Obj with id=" + id + ",timeout=" + timeoutSeconds);
    try
    {
      byte[] compressed = Snappy.compress(JSON.toJSONString(obj, new SerializerFeature[] { SerializerFeature.WriteClassName, SerializerFeature.DisableCircularReferenceDetect }).getBytes("UTF-8"));

      this.redisClusterCacher.getJedisCluster().set(id.getBytes(), compressed);
      Long timeoutResult = this.redisClusterCacher.getJedisCluster().expire(id.getBytes(), timeoutSeconds);
      if (logger.isDebugEnabled())
        logger.debug("The timeout setting result " + timeoutResult);
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void clear(String id)
  {
    if (this.isCluster)
      clearWithCluster(id);
    else
      clearWithNonCluster(id);
  }

  private void clearWithNonCluster(String id)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Del the Obj with id=" + id);
    }
    Jedis jedis = this.redisCacher.getJedisPool().getResource();
    try {
      jedis.del(id.getBytes());
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.redisCacher.getJedisPool().returnResource(jedis);
    }
  }

  private void clearWithCluster(String id) {
    if (logger.isDebugEnabled())
      logger.debug("Del the Obj with id=" + id);
    try
    {
      this.redisClusterCacher.getJedisCluster().del(id.getBytes());
    } catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void clear(List<String> ids)
  {
    if (this.isCluster)
      clearWithCluster(ids);
    else
      clearWithNonCluster(ids);
  }

  private void clearWithNonCluster(List<String> ids)
  {
    if (logger.isDebugEnabled()) {
      logger.debug("Del the Objs with ids=" + ids);
    }
    Jedis jedis = this.redisCacher.getJedisPool().getResource();
    try {
      for (String id : ids)
        jedis.del(id.getBytes());
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    } finally {
      this.redisCacher.getJedisPool().returnResource(jedis);
    }
  }

  private void clearWithCluster(List<String> ids) {
    if (logger.isDebugEnabled())
      logger.debug("Del the Objs with ids=" + ids);
    try
    {
      for (String id : ids)
        this.redisClusterCacher.getJedisCluster().del(id.getBytes());
    }
    catch (Exception e) {
      throw new CacheException("Redis operation error.", e);
    }
  }

  public void afterPropertiesSet() throws Exception
  {
    this.isCluster = this.redisConfig.isCluster();
    if (this.isCluster) {
      this.redisClusterCacher = new RedisClusterCacher();
      this.redisClusterCacher.setRedisConfig(this.redisConfig);
      this.redisClusterCacher.afterPropertiesSet();
    } else {
      this.redisCacher = new RedisCacher();
      this.redisCacher.setRedisConfig(this.redisConfig);
      this.redisCacher.afterPropertiesSet();
    }
  }

  public Object getOriginalCacher()
  {
    if (this.isCluster) {
      return this.redisClusterCacher.getOriginalCacher();
    }
    return this.redisCacher.getOriginalCacher();
  }

  public RedisConfig getRedisConfig()
  {
    return this.redisConfig;
  }

  public void setRedisConfig(RedisConfig redisConfig) {
    this.redisConfig = redisConfig;
  }
}
